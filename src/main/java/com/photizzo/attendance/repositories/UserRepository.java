package com.photizzo.attendance.repositories;

import com.photizzo.attendance.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
    
}
