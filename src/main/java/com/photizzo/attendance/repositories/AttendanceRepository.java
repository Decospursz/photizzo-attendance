package com.photizzo.attendance.repositories;

import com.photizzo.attendance.models.Attendance;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AttendanceRepository extends CrudRepository<List<Attendance>, String> {


}
