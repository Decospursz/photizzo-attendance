package com.photizzo.attendance.controllers;


import com.photizzo.attendance.dtos.input.AttendanceInputDTO;
import com.photizzo.attendance.dtos.input.RegisterUserInputDTO;
import com.photizzo.attendance.dtos.output.AttendanceResponseDTO;
import com.photizzo.attendance.dtos.output.UserResponseDTO;
import com.photizzo.attendance.services.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/v1")
@CrossOrigin(origins = "http://localhost:4200")
public class AppController extends Controller {
    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    @Autowired
    private AttendanceService attendanceService;


    @PostMapping("/user/register")
    private UserResponseDTO registerUser(@RequestBody @Valid RegisterUserInputDTO registerUserInputDTO){
        UserResponseDTO userResponse = attendanceService.createUser(registerUserInputDTO);
        updateHttpStatus(userResponse, response);
        return userResponse;
    }

    @PostMapping("/attendance/send")
    private AttendanceResponseDTO postAttendance(@RequestBody @Valid AttendanceInputDTO attendanceInputDTO){
        AttendanceResponseDTO attendanceResponseDTO = attendanceService.postAttendance(attendanceInputDTO);
        updateHttpStatus(attendanceResponseDTO, response);
        return attendanceResponseDTO;
    }




}
