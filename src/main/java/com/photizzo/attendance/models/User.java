package com.photizzo.attendance.models;

import com.photizzo.attendance.dtos.enums.Department;
import org.springframework.data.annotation.Id;

public class User {
    @Id
   private String id;

    private int seq;

    private int userId;

   private String firstName;

   private String lastName;

   private Department department;

    public User() {
    }

    public User(String firstName, String lastName, Department department) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
    }

    public User(int seq, String firstName, String lastName, Department department) {
        this.seq = seq;
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
