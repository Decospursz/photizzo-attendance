package com.photizzo.attendance.models;

import java.util.Date;

public class Attendance {
    private String date;

    private String time;

    private String userId;


    public Attendance(String date, String time, String userId) {
        this.date = date;
        this.time = time;
        this.userId = userId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
