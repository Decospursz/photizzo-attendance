package com.photizzo.attendance.dtos.enums;

public enum Department {
    SOFTWARE,SALES,FINANCE,ENGINEERING,PRODUCTION,INVENTORY
}
