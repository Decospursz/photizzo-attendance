package com.photizzo.attendance.dtos.enums;

public enum Status {
    SUCCESS, INTERNAL_ERROR, FAILED_VALIDATION, NOT_FOUND, INVALID_SERVICE_ERROR, CONFLICT, CREATED, NO_CONTENT;
}
