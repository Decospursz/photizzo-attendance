package com.photizzo.attendance.dtos.input;

import com.photizzo.attendance.dtos.enums.Department;

import javax.validation.constraints.NotNull;

public class RegisterUserInputDTO {
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private Department department;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
