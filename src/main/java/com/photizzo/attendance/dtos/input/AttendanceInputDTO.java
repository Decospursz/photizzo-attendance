package com.photizzo.attendance.dtos.input;

import com.photizzo.attendance.models.Attendance;

import java.util.List;

public class AttendanceInputDTO {
   private List<Attendance> attendanceList;

    public List<Attendance> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(List<Attendance> attendanceList) {
        this.attendanceList = attendanceList;
    }
}
