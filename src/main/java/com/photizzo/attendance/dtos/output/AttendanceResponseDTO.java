package com.photizzo.attendance.dtos.output;

import com.photizzo.attendance.dtos.enums.Status;
import com.photizzo.attendance.models.Attendance;

import java.util.List;

public class AttendanceResponseDTO extends StandardResponseDTO {
    private List<Attendance> attendanceList;

    public AttendanceResponseDTO(List<Attendance> attendanceList) {
        this.attendanceList = attendanceList;
    }

    public AttendanceResponseDTO(Status status, List<Attendance> attendanceList) {
        super(status);
        this.attendanceList = attendanceList;
    }

    public AttendanceResponseDTO(Status status) {
        super(status);
    }

    public List<Attendance> getAttendanceList() {
        return attendanceList;
    }

    public void setAttendanceList(List<Attendance> attendanceList) {
        this.attendanceList = attendanceList;
    }
}
