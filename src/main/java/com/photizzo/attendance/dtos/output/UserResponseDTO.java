package com.photizzo.attendance.dtos.output;

import com.photizzo.attendance.dtos.enums.Status;
import com.photizzo.attendance.models.User;

public class UserResponseDTO extends StandardResponseDTO{
    private User user;

    public UserResponseDTO(User user) {
        this.user = user;
    }

    public UserResponseDTO(Status status, User user) {
        super(status);
        this.user = user;
    }

    public UserResponseDTO(Status status) {
        super(status);
    }



    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
