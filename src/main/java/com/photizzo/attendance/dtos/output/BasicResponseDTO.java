package com.photizzo.attendance.dtos.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.photizzo.attendance.dtos.enums.Status;

public class BasicResponseDTO {
    @JsonProperty
    private Status status;
    @JsonProperty
    private Object data;

    public BasicResponseDTO() {
    }

    public BasicResponseDTO(Status status) {
        this.status = status;
    }

    public BasicResponseDTO(Status status, Object data) {
        this.status = status;
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
