package com.photizzo.attendance.dtos.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.photizzo.attendance.dtos.enums.Status;

public class StandardResponseDTO {
    @JsonProperty
    protected Status status;

    public StandardResponseDTO() {
    }

    StandardResponseDTO(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
