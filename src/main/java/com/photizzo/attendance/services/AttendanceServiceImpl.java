package com.photizzo.attendance.services;

import com.photizzo.attendance.dtos.enums.Department;
import com.photizzo.attendance.dtos.enums.Status;
import com.photizzo.attendance.dtos.input.AttendanceInputDTO;
import com.photizzo.attendance.dtos.input.RegisterUserInputDTO;
import com.photizzo.attendance.dtos.input.SendToHardwareInputDTO;
import com.photizzo.attendance.dtos.output.AttendanceResponseDTO;
import com.photizzo.attendance.dtos.output.UserResponseDTO;
import com.photizzo.attendance.models.Attendance;
import com.photizzo.attendance.models.User;
import com.photizzo.attendance.repositories.AttendanceRepository;
import com.photizzo.attendance.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 *
 * @author Ernest David
 */
@Service("attendanceService")
public class AttendanceServiceImpl implements AttendanceService {
    @Autowired
    private AttendanceRepository attendanceRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NextSequenceService nextSequenceService;

    private RestTemplate restTemplate = new RestTemplate();

    private static final String SERVICE_NAME = "AttendanceService";

    @Override
    public UserResponseDTO createUser(RegisterUserInputDTO dto) {
        try{
            User newUser = createNewUser(dto);
            userRepository.save(newUser);

            UserResponseDTO userResponseDTO = new UserResponseDTO( Status.SUCCESS, newUser);
            int userId = userResponseDTO.getUser().getUserId();
            String userName = userResponseDTO.getUser().getFirstName() + " " + userResponseDTO.getUser().getLastName();
            sendIdToHardwareClient(userId, userName);
            return userResponseDTO;
        } catch (Exception e){
            logError(SERVICE_NAME, "createUser", e);
            return new UserResponseDTO(Status.INTERNAL_ERROR);
        }

    }

    private void sendIdToHardwareClient(int userId, String userName) {
        SendToHardwareInputDTO dto = new SendToHardwareInputDTO();
        dto.setId(userId);
        dto.setUserName(userName);
        HttpEntity requestPayload = new HttpEntity<>(dto);

        String url = "http://192.168.15.50:334";

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(url, requestPayload, String.class);
    }




    @Override
    public AttendanceResponseDTO postAttendance(AttendanceInputDTO dto) {
        try{
            List<Attendance> attendanceList = dto.getAttendanceList();
            attendanceRepository.save(attendanceList);
            return new AttendanceResponseDTO(Status.SUCCESS, attendanceList);

        } catch (Exception e){
            logError(SERVICE_NAME, "postAttendance", e);
            return new AttendanceResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private User createNewUser(RegisterUserInputDTO dto) {
        for ( Department dept : Department.values()) {
            if (dept.name().equals(dto.getDepartment().name())){
                User user = new User();
                user.setFirstName(dto.getFirstName());
                user.setLastName(dto.getLastName());
                user.setDepartment(dto.getDepartment());
                user.setUserId(nextSequenceService.getNextSequence("user"));
                return user;
            }
        }
        return null;
    }

    void logError(String serviceName, String methodName, Exception ex) {
        ex.printStackTrace(); //fluentd, logstash
    }
}
