package com.photizzo.attendance.services;

import com.photizzo.attendance.dtos.input.AttendanceInputDTO;
import com.photizzo.attendance.dtos.input.RegisterUserInputDTO;
import com.photizzo.attendance.dtos.output.AttendanceResponseDTO;
import com.photizzo.attendance.dtos.output.UserResponseDTO;

public interface AttendanceService {

    UserResponseDTO createUser(RegisterUserInputDTO registerUserInputDTO);

    AttendanceResponseDTO postAttendance(AttendanceInputDTO attendanceInputDTO);
}
